<?php


namespace App\Http\Controllers;


use App\Category;
use App\Post;
use Illuminate\Http\Request;

class CategoryController
{
    /**
     * @return mixed
     */
    public function index()
    {
        $categories = Category::all();
        return view('categories.index')->withCategories($categories);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $category = new Category();
        $category->name = $request->category;

        $category->save();

        return redirect()->back();

    }

}
