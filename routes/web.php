<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('posts', 'PostController')->middleware('auth');
Route::get('/index', 'PostController@index')->name('index');
Route::resource('categories', 'CategoryController');
//Route::get('/index', 'PostController@index')->name('index');
//Route::get('posts/create', 'PostController@create')->name('create');
//Route::post('posts/store', 'PostController@store')->name('store');


