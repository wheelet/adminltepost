@extends('home')

@section('title', '| Create New Post')

@section('stylesheets')

@endsection

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1>Create New Post</h1>
            <hr>
            {{ Form::open(array('route' => 'categories.store')) }}
            {{ Form::label('Category', 'Category:') }}
            {{ Form::text('category', null, array('class' => 'form-control', 'required' => '', 'maxlength' => '255')) }}
            {{ Form::submit('Create Post', array('class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top: 20px;')) }}
            {!! Form::close() !!}
        </div>
    </div>

@endsection



