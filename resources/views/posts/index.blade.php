@extends('home')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                <th>#</th>
                <th>Title</th>
                <th>Body</th>
                <th>Name</th>
                <th>Created At</th>
                <th></th>
                </thead>

                <tbody>

                @foreach ($posts as $post)

                    <tr>
                        <th>{{ $post->id }}</th>
                        <td>{{ $post->title }}</td>
                        <td>{{ $post->body }}</td>
                        <td>{{ $post->category->name }}</td>
                        <td>{{ date('M j, Y', strtotime($post->created_at)) }}</td>
                    </tr>

                @endforeach

                </tbody>
            </table>
        </div>
    </div>

@stop
